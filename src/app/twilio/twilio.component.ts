import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../api.service';
import { ConfigService } from '../config.service';
import { ToastService } from '../toast.service';
import * as _ from 'lodash';
import { NgModel, Validators } from '@angular/forms';
import { FormService } from '../form.service';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-twilio',
  templateUrl: './twilio.component.html',
  styleUrls: ['./twilio.component.scss']
})
export class TwilioComponent implements OnInit {
  form;
  domain;
  success;
  saving: boolean;

  spinner = faSpinner;

  constructor(
    private api: ApiService,
    private toast: ToastService,
    protected formService: FormService,
    public config: ConfigService
  ) { }

  ngOnInit(): void {
    this.form = this.createForm();
    this.domain = document.location.hostname;
  }

  createForm() {
    return this.formService.createFormGroup({
      obj: {
        to_number: ['text', [Validators.required]],
        body: ['text', [Validators.required]],
      }
    });
  }

  submit() {
    if (!this.form.trySubmit()) return;

    this.api.post({
      endpoint: `${this.config.xanoApiUrl}/twilio/validate`,
      params: {
        ...this.form.value
      },
      stateFunc: state => this.saving = state
    })
    .subscribe((response:any) => {
      this.toast.success("Success");
      this.success = true;
    }, (err) => {
      this.toast.error(_.get(err, "error.message") || "An unknown error has occured.");
    });
  }
}
